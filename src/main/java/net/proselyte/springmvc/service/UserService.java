package net.proselyte.springmvc.service;

import net.proselyte.springmvc.entity.UsersEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import repositories.UsersRepository;


@Component("userservice")
@Service
public class UserService implements  IUserServive {

    private  UsersRepository repository;

    @Transactional
    public void delete (Long id){
        repository.delete(id);
    }

    public Page<UsersEntity> findAll(int page, int size) {
        Pageable pageable = new PageRequest(page, size, new Sort(
                Sort.Direction.DESC, "id"));
        Page<UsersEntity> persons = (Page<UsersEntity>) repository.findAll();
        return persons;
    }


    public void save(UsersEntity user){
        repository.save(user);
    }

    public void setRepository(UsersRepository repository)
    {
        this.repository = repository;
    }

    @Transactional(readOnly = true)
    public UsersEntity findById(Long id) {
        UsersEntity person = repository.findOne(id);
        return person;
    }

    @Transactional
    public UsersEntity insert(UsersEntity person) {
        return repository.save(person);
    }

    @Transactional
    public UsersEntity update(UsersEntity person) {
        return repository.save(person);
    }

    @Transactional
    public void deleteById(Long id) {
        repository.delete(id);
    }

}
