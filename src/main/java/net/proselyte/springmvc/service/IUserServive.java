package net.proselyte.springmvc.service;

import net.proselyte.springmvc.entity.UsersEntity;
import org.springframework.data.domain.Page;

public interface IUserServive {
    Page<UsersEntity> findAll(int page, int size);

    UsersEntity findById(Long id);

    UsersEntity insert(UsersEntity person);

    UsersEntity update(UsersEntity person);

    void deleteById(Long id);
}
