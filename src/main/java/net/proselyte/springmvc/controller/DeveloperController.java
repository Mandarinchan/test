package net.proselyte.springmvc.controller;


import net.proselyte.springmvc.entity.UsersEntity;

import org.springframework.stereotype.Controller;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.servlet.ModelAndView;


@RequestMapping("user")
@Controller
public class DeveloperController {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "/index";
    }

    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public String viewUser() {
        return "/view";
    }

    @RequestMapping(value = "developer", method = RequestMethod.GET)
    public ModelAndView developer() {
        return new ModelAndView("developer", "command", new UsersEntity());
    }

    @RequestMapping(value = "/addDeveloper", method = RequestMethod.POST)
    public String addStudent(@ModelAttribute("mvc-dispatcher") UsersEntity developer,
                             ModelMap model) {
        model.addAttribute("id",developer.getId());
        model.addAttribute("firstname", developer.getFirstname());
        model.addAttribute("lastname", developer.getLastname());
        model.addAttribute("middlename", developer.getMiddlename());
        return "result";
    }


}
